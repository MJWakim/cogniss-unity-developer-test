﻿namespace Developer.Screen
{
    using UnityEngine;

    using System.Threading.Tasks;

    /// <inheritdoc />
    /// <summary>
    /// Controller for the screen that displays a <see cref="Developer.Component.TrafficLight" />.
    /// </summary>
    public sealed class LightScreen : Core.ScreenAbstract
    {
        private Developer.Component.TrafficLight lights = null;

        private void Awake()
        {
            GameObject trafficLightPrefab = Resources.Load<GameObject>("Prefabs/Component/TrafficLight/TrafficLight");
            GameObject trafficLightInstance = Instantiate(trafficLightPrefab, transform.Find("TrafficLightHolder"));
            lights = trafficLightInstance.AddComponent<Developer.Component.TrafficLight>();
            lights.SetState();

            Run();

            
        }

        //function gests light value and Sets light colour
        private async void Run()
        {
            int time = 0;
            while (time < 100)
            {
                await Task.Delay(1000);
                time = await Test.NetworkService.GetCurrentLightValue();
                print(time);

                if (time % 3 == 0 && time % 5 == 0)
                {
                    lights.SetState(Test.LightColor.Green);
                }
                else if (time % 5 == 0)
                {
                    lights.SetState(Test.LightColor.Yellow);
                }
                else if (time % 3 == 0)
                {
                    lights.SetState(Test.LightColor.Red);
                }
                else
                {
                    lights.SetState();
                }
            }

            FinaleScreen();
        }

        //move to next scene
        private void FinaleScreen()
        {
            _app.ChangeScreen<FinaleScreen>();
        }


        //TODO: According the rules, turn the lights on and off...
        //Hint: The networking functionality is inside the "Test" namespace.

    }
}