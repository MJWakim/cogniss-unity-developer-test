﻿namespace Developer.Screen
{
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.SceneManagement;

    public sealed class FinaleScreen : Core.ScreenAbstract
    {
        Camera mainCam;
        Button RestartButton;

        void Awake()
        {
            //using camera solid colour for background
            mainCam = Camera.main;
            GameObject Restart = GameObject.FindGameObjectWithTag("Button");
            RestartButton = Restart.GetComponent<Button>();
            RestartButton.onClick.AddListener(RestartApp);
            PlatformCheck();
        }

        //platform dependent compilation for background colour
        void PlatformCheck()
        {
#if UNITY_EDITOR
            mainCam.backgroundColor = Color.yellow;
#elif UNITY_IOS
            mainCam.backgroundColor = Color.green;
#elif UNITY_ANDROID
            mainCam.backgroundColor = Color.red;
#endif

        }

        //reload scene
        public void RestartApp()
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
